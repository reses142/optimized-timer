import React, { PureComponent, Component } from "react";
import {
  View,
  Alert,
  StyleSheet,
  TouchableOpacity,
  ScrollView
} from "react-native";
import {
  Grid,
  Row,
  Col,
  Text,
  Button,
  Container,
  Content,
  Item,
  Picker,
  Input
} from "native-base";
import moment from "moment";

class TopBtns extends Component {
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <View style={styles.btnBlock}>
        <Button style={styles.Btn} onPress={this.props.startTimer}>
          <Text style={{ color: "#fff", fontSize: 24 }}>START</Text>
        </Button>

        <View
          style={{ backgroundColor: "#fff", width: "0.5%", height: "100%" }}
        />

        <Button style={styles.Btn} onPress={this.props.addLap}>
          <Text style={{ color: "#fff", fontSize: 24 }}>LAP</Text>
        </Button>
      </View>
    );
  }
}

class BottomBtns extends Component {
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <View style={{ flexDirection: "row", height: 60 }}>
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.saveGroupBtn}
          onPress={this.props.stopTimer}
        >
          <Text style={styles.saveTitle}>STOP</Text>
        </TouchableOpacity>

        <View
          style={{ backgroundColor: "#fff", height: "100%", width: "0.5%" }}
        />

        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.saveGroupBtn}
          onPress={this.props.resetRace}
        >
          <Text style={styles.saveTitle}>RESET</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

class MainContent extends Component {
  state = {
    currentLaps: []
  };

  shouldComponentUpdate(nextProps, nextState) {
    const { currentLaps } = this.state;
    if (currentLaps.length !== nextProps.laps.length) {
      this.setState({
        currentLaps: nextProps.laps
      });

      return true;
    }

    return false;
  }

  _renderLaps = laps => {
    return laps.map((result, i) => {
      return (
        <Row
          style={{
            height: 70,
            flexDirection: "row",
            justifyContent: "flex-start",
            alignItems: "center"
          }}
          key={i}
        >
          <Text style={{ marginHorizontal: 10, ...styles.contentText }}>
            {result.place}
          </Text>

          <Text style={styles.contentText}>{result.time}</Text>

          {result.existMemberData && (
            <Fragment>
              <Text style={{ marginHorizontal: 5, ...styles.contentText }}>
                {result.memberData.fullName}
              </Text>
              <Text style={{ marginHorizontal: 5, ...styles.contentText }}>
                Bib# {result.memberData.bib}
              </Text>
            </Fragment>
          )}

          <Button style={styles.BtnContent}>
            <Text style={{ color: "#fff" }}>
              {result.existMemberData ? "RESCAN" : "SCAN BIB"}
            </Text>
          </Button>

          <Button style={styles.BtnContent}>
            <Text style={{ color: "#fff" }}>ENTER BIB</Text>
          </Button>
        </Row>
      );
    });
  };

  render() {
    return (
      <ScrollView showsVerticalScrollIndicator={false} overScrollMode="never">
        <Content
          style={{ backgroundColor: "#fff" }}
          showsVerticalScrollIndicator={false}
          overScrollMode="never"
          horizontal={true}
          alwaysBounceHorizontal={true}
        >
          <Grid style={{ flexDirection: "column" }}>
            {this._renderLaps(this.state.currentLaps)}
          </Grid>
        </Content>
      </ScrollView>
    );
  }
}

export default class App extends PureComponent {
  state = {
    laps: [],
    position: 0,
    miliseconds: 0,
    seconds: 0,
    minutes: 0,
    stoppedTime: {
      miliseconds: 0,
      seconds: 0,
      minutes: 0
    }
  };

  startTimer = () => {
    // if (this.timer) return;
    const now = moment();

    const delay = 10;
    this.timer = setInterval(() => {
      const delayedNow = moment()
        // .add(this.stoppedTime.miliseconds, "milliseconds")
        .add(this.state.stoppedTime.seconds, "seconds")
        .add(this.state.stoppedTime.minutes, "minutes");
      const delay = delayedNow.diff(now);
      const duration = moment.duration(delay);

      this.setState({
        miliseconds: duration._data.milliseconds,
        seconds: duration._data.seconds,
        minutes: duration._data.minutes
      });
    }, delay);
  };

  stopTimer = () => {
    clearInterval(this.timer);
    this.setState({
      stoppedTime: {
        miliseconds: this.state.miliseconds,
        seconds: this.state.seconds,
        minutes: this.state.minutes
      }
    });

    this.timer = null;
  };

  resetRace = () => {
    this.setState({
      laps: [],
      position: 0,
      miliseconds: 0,
      seconds: 0,
      minutes: 0,
      stoppedTime: {
        miliseconds: 0,
        seconds: 0,
        minutes: 0
      }
    });
  };

  addLap = () => {
    // if (!this.timer) return;

    const position = this.state.position + 1;
    const time = `${this.state.minutes
      .toString()
      .padStart(1, 0)}:${this.state.seconds
      .toString()
      .padStart(2, 0)}:${this.state.miliseconds.toString().padStart(3, 0)}`;
    const laps = [...this.state.laps];

    laps.unshift({
      place: position,
      time: time,
      memberData: { fullName: null, bib: null },
      existMemberData: false
    });

    this.setState({
      laps,
      position
    });
  };

  formatForTime = (time, zeroCount) => {
    return time.toString().padStart(zeroCount, 0);
  };

  render() {
    const {
      state: { laps, miliseconds, seconds, minutes },
      formatForTime,
      _renderLaps,
      startTimer,
      stopTimer,
      addLap,
      resetRace
    } = this;

    return (
      <Container>
        <View>
          <View style={styles.timeBlock}>
            <Text style={{ color: "#14009b", fontSize: 55 }}>
              {formatForTime(minutes, 1)}:
            </Text>
            <Text style={{ color: "#14009b", fontSize: 55 }}>
              {formatForTime(seconds, 2)}:
            </Text>
            <Text style={{ color: "#14009b", fontSize: 55 }}>
              {formatForTime(miliseconds, 3)}
            </Text>
          </View>

          <TopBtns startTimer={startTimer} addLap={addLap} />
        </View>

        <MainContent laps={laps} />

        <BottomBtns resetRace={resetRace} stopTimer={stopTimer} />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  timeBlock: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  modalWindow: {
    width: "100%",
    backgroundColor: "#fff",
    marginLeft: "auto",
    marginRight: "auto"
  },
  BtnContent: {
    backgroundColor: "rgba(184, 8, 17, 1)",
    paddingLeft: 5,
    paddingRight: 5,
    borderRadius: 20,
    marginVertical: 2,
    marginHorizontal: 5
  },
  btnBlock: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginBottom: 20,
    borderBottomColor: "#a0a0a0",
    borderBottomWidth: 2
  },
  Btn: {
    backgroundColor: "rgba(184, 8, 17, 1)",
    width: "49.75%",
    flex: 0,
    borderRadius: 0,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  saveGroupBtn: {
    backgroundColor: "rgba(184, 8, 17, 1)",
    width: "49.75%",
    height: 60,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  saveTitle: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center",
    textAlignVertical: "center",
    alignSelf: "center"
  },
  contentText: {
    textAlignVertical: "center",
    color: "#000",
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center",
    marginBottom: "auto",
    marginTop: 15,
    alignSelf: "center"
  }
});
